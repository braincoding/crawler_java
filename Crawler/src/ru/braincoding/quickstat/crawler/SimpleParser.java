package ru.braincoding.quickstat.crawler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SimpleParser implements Parser {
	private static final String[] TAGS = {"h1", "h2", "h3", "h4", "h5", "h6", "p"}; 

	@Override
	public int parse(String page, Collection<String> keyWords) {
		int counter = 0;
		Document doc = Jsoup.parse(page);
		Elements content = new Elements();
		for (String string : TAGS) {
			content.addAll(doc.getElementsByTag(string));
		}
		ArrayList<String> words = new ArrayList<>(getWords(content));

		for (String word : words) {
			if (isKeyWord(word, keyWords)) {
				counter++;
			}
		}
		return counter;
	}

	private boolean isKeyWord(String word, Collection<String> keyWords) {
		boolean result = false;
		for (String keyWord : keyWords) {
			if (word.equals(keyWord)) {
				result = true;
				break;
			}
		}
		return result;
	}


	private ArrayList<String> getWords(Elements content) {
		ArrayList<String> words = new ArrayList<String>();
		for (Element element : content) {
			words.addAll(Arrays.asList(element.text().split("[\\s]")));
		}
		return words;
	}

}
