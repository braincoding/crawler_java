package ru.braincoding.quickstat.crawler.robots;


import java.io.*;
import java.util.zip.GZIPInputStream;

public class GZIPparser {

    public static String parse(String inputSource) throws IOException {
        String FILE = inputSource;
        String result = null;

        FileInputStream fin = new FileInputStream(FILE);
        GZIPInputStream gzis = new GZIPInputStream(fin);
        InputStreamReader xover = new InputStreamReader(gzis);
        BufferedReader is = new BufferedReader(xover);

        String line;

        while ((line = is.readLine()) != null)
            result = result + line + "\n";
        return result;
    }
}
