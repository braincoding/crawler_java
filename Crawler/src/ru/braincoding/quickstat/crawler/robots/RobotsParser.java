package ru.braincoding.quickstat.crawler.robots;

public class RobotsParser {
	public static String parse(String file) {
		String result ="";
		String[] words = file.split("\\s");
		int sitemapUrlIndex = -1;
		for (int i = 0; i < words.length; i++) {
			if (words[i].contains("Sitemap")) {
				sitemapUrlIndex = i + 1; 
				break;
			}
			
		}
		if ((sitemapUrlIndex > -1) && (sitemapUrlIndex < words.length)) {
			result = words[sitemapUrlIndex];
		}

		return result;
	}

}
