package ru.braincoding.quickstat.crawler;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ru.braincoding.quickstat.crawler.models.Page;

public class SitemapParser {
	

	private static DocumentBuilderFactory docBuilderFactory = null;
	private static DocumentBuilder db = null;
	
	
	public static void init() {
		/* Получаем экземпляр фабрики парсеров для разбора ХМЛ
		 * и отменяем валидацию ХМЛ */
		docBuilderFactory = DocumentBuilderFactory.newInstance(); 
		docBuilderFactory.setValidating(false);
		try {
			db = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Collection<Page> parseSiteMap(InputStream src, Page srcPage) {
		
		
	    //DocumentBuilder db;
	    Collection<Page> result = new ArrayList<>();

	    /* создаем ХМЛ документ из скачанной страницы */
	    Document doc = null;
	    try {
	    	doc = db.parse(src);
	    } catch (SAXException | IOException e) {
	    	// TODO Auto-generated catch block
	    	e.printStackTrace();
	    } 

	    if (doc != null) {
	    	/* создаем список нод для парсинга */
	    	Collection<Node> pagesUrls = new ArrayList<>();

	    	/* берем из документа все ноды с тегами "sitemap" и "url" */
	    	addAllNodes(doc.getElementsByTagName("sitemap"), pagesUrls);
	    	addAllNodes(doc.getElementsByTagName("url"), pagesUrls);


	    	/* вытаскиваем из нод адреса страниц и время добавления/изменения
	    	 * снабжаем адреса страниц соответствующим siteId и
	    	 * закидываем адреса страниц в результирующий список  */
	    	for (Node node : pagesUrls){
	    		NodeList nodes = node.getChildNodes();
	    		Page page = new Page();
	    		page.siteId = srcPage.siteId;
	    		page.url = getUrl(nodes);
	    		page.foundDateTime = getDate(nodes);
	    		result.add(page);
	    	}
	    }
		
		/* возвращаем результат */
		return result;

		// TODO LOG
		
	}

	private static void addAllNodes(NodeList nodeList, Collection<Node> result) {
		int length = nodeList.getLength();
		for (int i = 0; i < length; i++) {
			result.add(nodeList.item(i));
		}
		
	}

	private static Timestamp getDate(NodeList nodes) {
		LocalDateTime locTimeStamp = LocalDateTime.now();
		String timeString = null;
		int length = nodes.getLength();
		for (int i = 0; i < length; i++) {
			if ("lastmod".equals(nodes.item(i).getNodeName())) {
				timeString = nodes.item(i).getTextContent().trim();
				break;
			}
			
		}
		if (timeString != null) {
			try {
				locTimeStamp = LocalDateTime.parse(timeString, 
						DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			} catch (DateTimeParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return Timestamp.valueOf(locTimeStamp);
	}

	private static String getUrl(NodeList nodes) {
		String result = null;
		int length = nodes.getLength();
		for (int i = 0; i < length; i++) {
			if ("loc".equals(nodes.item(i).getNodeName())) {
				result = nodes.item(i).getTextContent().trim();
				break;
			}
			
		}
		return result;
	}
	

}
