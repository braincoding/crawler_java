package ru.braincoding.quickstat.crawler;

import java.io.IOException;
import java.io.PrintWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * This class is used to download pages
 * @author Sergey Sinitsyn
 *
 */
public class Downloader implements IDownloader {
	
/**
 * Returns a String containing downloaded page
 * @param url the String contain page's URL
 * @return the String contains downloaded page or empty String if any error
 */
 @Override
public String download(String url) {
		String result = "";
		if (url != null && !url.isEmpty()) {
			Document doc = null;
			try {
				doc = Jsoup.connect(url).get();
			} catch (IOException e) {
				// TODO Change PrintWriter
				e.printStackTrace(new PrintWriter(System.err));
			}
			if (doc != null) {
				result = doc.outerHtml();
			}
			
		}
		return result;
	}

}
