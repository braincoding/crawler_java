package ru.braincoding.quickstat.crawler.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import ru.braincoding.quickstat.crawler.models.Keyword;
import ru.braincoding.quickstat.crawler.models.Page;
import ru.braincoding.quickstat.crawler.models.Person;
import ru.braincoding.quickstat.crawler.models.PersonPageRank;
import ru.braincoding.quickstat.crawler.models.Site;


/**
 * Класс для работы с базой данных
 * @author Sergey Sinitsyn
 *
 */

public class DbStorage implements IDbStorage {
	private Connection conn = null;
	
	public DbStorage(Connection conn) {
		if (conn != null) {
			this.conn = conn;
		} else {
			System.out.println("EE: Wrong connection");
			System.exit(10);
			// TODO Create log
		}
	}
	
	/**
	 * Выбирает сайты из базы данных для которых еще нет ни одной страницы
	 * 
	 * @return список (коллекцию) сайтов для которых нет ни одной страниы 
	 * или пустой список если таких сайтов нет
	 */
	@Override
	public Collection<Site> getNewSites() {
		/* текст запроса */
		String sqlQuery = "SELECT * FROM " + Site.TABLE + " "
				+ "WHERE 0 = (SELECT COUNT(*) "
				+ "FROM " + Page.TABLE + " "
				+ "WHERE " + Page.TABLE + "." + Page.Columns.SITE_ID + " = "
				+ Site.TABLE + "." + Site.Columns.ID + ")";

		/* создаем результирующий список */
		ArrayList<Site> sites = new ArrayList<>();
		try {
			conn.setAutoCommit(false);
			
			/* формируем запрос */
			java.sql.PreparedStatement stm = conn.prepareStatement(sqlQuery);
			
			/* выполняем запрос и добавляем то что вернулось в результирующий список */
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Site site = new Site();
				site.id = res.getLong(Site.Columns.ID);
				site.name = res.getString(Site.Columns.NAME);
				sites.add(site);
						
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return sites;
	}

	/* заглушка скорее всего метод не понадобится */
	public boolean addPage(long id, String createRobots) {
		// TODO Auto-generated method stub
		return false;
		
	}

	/**
	 * Выбирает из базы данных страницы у которых время последнего сканирования равно 0000-00-00 00:00:00
	 * @param limit максимальное количество страниц
	 * @return список страниц или пустой список если таких страниц нет
	 */
	@Override
	public Collection<Page> getNewPages(int limit) {
		Collection<Page> result = new ArrayList<>();
		String sqlQuery = "SELECT * FROM " + Page.TABLE
				+ " WHERE " + Page.Columns.SCAN + "='0000-00-00 00:00:00'"
				+ " LIMIT ?;";
/*		String sqlQuery = "SELECT * FROM `test2_pages` WHERE `last_scan_date`='0000-00-00 00:00:00' LIMIT ?;";*/		try {
			conn.setAutoCommit(false);
			java.sql.PreparedStatement stm = conn.prepareStatement(sqlQuery);
			stm.setInt(1, limit);
			
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Page pg = new Page();
				pg.id = res.getLong(Page.Columns.ID);
				pg.siteId = res.getLong(Page.Columns.SITE_ID);
				pg.url = res.getString(Page.Columns.URL);
				result.add(pg);
						
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return result;
	}
	
	/* Заглушка */
	@Override
	public Collection<Page> getPages(int limit) {
		Collection<Page> pages = new ArrayList<>();
		return pages;
	}

	/**
	 * Метод для получения страниц которые обходились раньше некоторого количества дней 
	 * до настоящего момента кроме дат, у которых дата последнего сканирования
	 * равна 0000-00-00 00:00:00
	 * @param days количество дней, такое что выбираются страницы у которых дата
	 * последнего сканирования раньше чем текущая дата минус число дней равное days
	 * @param limit максимальное число страниц
	 * @return возвращает список страниц у которых дата последнего сканирования раньше чем за days дней 
	 * до текущей
	 */
	@Override
	public Collection<Page> getPagesBefore(int days, int limit) {
		/* текст запроса */
		String sqlQuery = "SELECT * FROM " + Page.TABLE
				+ " WHERE " + Page.Columns.SCAN
				+ " BETWEEN '1900-01-01 00:00:00'"
				+ " AND (SELECT NOW( ) - INTERVAL ? DAY)"
				+ " ORDER BY " + Page.Columns.SCAN + " ASC LIMIT ?;";
/*		String sqlQuery = "SELECT * FROM `test2_pages` "
				+ "WHERE `last_scan_date` BETWEEN '1900-01-01 00:00:00'"
				+ "AND (SELECT NOW( ) - INTERVAL ? DAY)"
				+ "ORDER BY `last_scan_date` ASC LIMIT ?;";*/
		Collection<Page> pages = new ArrayList<>();
		try {
			conn.setAutoCommit(false);
			java.sql.PreparedStatement stm = conn.prepareStatement(sqlQuery);
			stm.setInt(1, days);
			stm.setInt(2, limit);
			
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Page pg = new Page();
				pg.id = res.getLong(Page.Columns.ID);
				pg.siteId = res.getLong(Page.Columns.SITE_ID);
				pg.url = res.getString(Page.Columns.URL);
				pg.lastScanDate = res.getTimestamp(Page.Columns.SCAN);
				pages.add(pg);
						
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return pages;
	}

	/**
	 * Обновляет дату сканирования 
	 * @param pages список страниц
	 * @return количество страниц у которых была обновлена дата
	 */
	@Override
	public int updateLastScanDate(Collection<Page> pages) {
		
		int result = 0;
		
		/* текст запроса */
		String sqlQuery = "UPDATE " + Page.TABLE
				+ " SET " + Page.Columns.SCAN + " = NOW()"
				+ " WHERE " + Page.Columns.ID + " = ?;";
		/*String sqlQuery = "UPDATE `test2_pages` SET `last_scan_date`=NOW() WHERE `id`=?;";*/
		try {
			conn.setAutoCommit(false);
			
			/* формируем пакетный запрос */
			java.sql.PreparedStatement stm = conn.prepareStatement(sqlQuery);
			for (Page page : pages) {
				stm.setLong(1, page.id);
				stm.addBatch();
			}
			/* выполняем запрос, количество страниц записываем в результат */
			result = stm.executeBatch().length;
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Change update when create sitemap parser
		return result;
	}

	/**
	 * Выбирает личности из базы
	 * @return список личностей
	 */
	@Override
	public Collection<Person> getPersons() {
		Collection<Person> result = new ArrayList<>();
		
		/* текст запроса */
		String sqlQuery = "SELECT * FROM " + Person.TABLE + " ;";
		try {
			conn.setAutoCommit(false);
			java.sql.PreparedStatement stm = conn.prepareStatement(sqlQuery);
			
			/* выполняем запрос */
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				/* добавляем личности в результат */
				Person person = new Person();
				person.id = res.getLong(Person.Columns.ID);
				person.name = res.getString(Person.Columns.NAME);
				result.add(person);
						
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return result;
	}

	/**
	 * Выбирает из базы ключевые слова соответствующие личности person
	 * @param person личность для которой нужно выбрать ключевые слова
	 * @return список ключевых слов
	 */
	@Override
	public Collection<String> getKeyWords(Person person) {
		Collection<String> result = new ArrayList<>();
		
		/* текст запроса */
		String sqlQuery = "SELECT * FROM " + Keyword.TABLE
				+ " WHERE " + Keyword.Columns.PERSON_ID + " = ?;";
		try {
			conn.setAutoCommit(false);
			
			/* создаем запрос */
			java.sql.PreparedStatement stm = conn.prepareStatement(sqlQuery);
			
			/* подставляем id личности в запрос */
			stm.setLong(1, person.id);
			
			/* выполняем запрос */
			ResultSet res = stm.executeQuery();
			
			/* добавляем то что вернулось к результату */
			while (res.next()) {
				result.add(res.getString(2));
						
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return result;
	}
	
	/**
	 * Нужно будет переделать! Передавать не отдельно персону страницу и рейтинг
	 * а список PersonPageRank, соотвественно добавлять не по одной записи а пакетом
	 * Добавляет запись в таблицу test_person_page_rank
	 * @param person персона
	 * @param page страница
	 * @param rank рейтинг
	 * @return true если добавление прошло успешно, иначе false
	 */
	@Override
	public boolean updateRank(Person person, Page page, int rank) {
		int result = 0;
		
		/* текст запроса */
		String sqlQuery = "INSERT INTO " + PersonPageRank.TABLE + " ("
				+ PersonPageRank.Columns.PERSON_ID + ", "
				+ PersonPageRank.Columns.PAGE_ID + ", "
				+ PersonPageRank.Columns.RANK + ")"
				+ " VALUES (?, ?, ?);";
/*		String sqlQuery = "INSERT INTO `test_person_page_rank`(person_id, page_id, rank)"
				+ " VALUES (?, ?, ?);";*/
		try {
			conn.setAutoCommit(false);
			
			/* создаем запрос */
			java.sql.PreparedStatement stm = conn.prepareStatement(sqlQuery);
			
			/* устанавливаем параметры */
			stm.setLong(1, person.id);
			stm.setLong(2, page.id);
			stm.setInt(3, rank);
			
			/* выполняем запрос */
			result = stm.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return result == 1;
		
	}
	
	/* Заглушка */
	@Override
	public int updateRank(Collection<PersonPageRank> ranks) {
		int result = 0;
		return result;
	}
	
	/**
	 * Добавляет страницы в базу данных
	 * @param pages список страниц для добавления
	 * @return количество добавленных страниц
	 */
	@Override
	public int addPages(Collection<Page> pages) {
		int result = 0;
		/* текст запроса */
		String sqlQuery = "INSERT IGNORE INTO "+ Page.TABLE + "("
				+ Page.Columns.SITE_ID + ", " 
				+ Page.Columns.URL + ", " 
				+ Page.Columns.FOUND + ", " 
				+ Page.Columns.HASH + ") "
				+ "VALUES (?, ?, ?, SHA2(?, 256));";
		/*String sqlQuery = "INSERT IGNORE INTO test2_pages(site_id, url, found_date_time, hash)"
				+ " VALUES (?, ?, ?, SHA2(?, 256));";*/
		try {
			conn.setAutoCommit(false);
			
			
			java.sql.PreparedStatement stm = conn.prepareStatement(sqlQuery);
			
			/* устанавливаем параметры в соотвествии с каждой страницей и добавляем в пакет */
			for (Page page : pages) {
				stm.setLong(1, page.siteId);
				stm.setString(2, page.url);
				stm.setTimestamp(3, page.foundDateTime);
				stm.setString(4, page.url);
				stm.addBatch();
			}
			
			/* выполняем запрос, записываем в результат количество строк */
			result = stm.executeBatch().length;
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
		// TODO Auto-generated method stub
	
	}
	
}
