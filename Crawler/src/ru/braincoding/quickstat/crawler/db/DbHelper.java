package ru.braincoding.quickstat.crawler.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbHelper {
	private static Connection conn = null;
	private static String user = "gb_tstcrawler";
	private static String password = "6zac85cfajk";
	private static String url = "jdbc:mysql://quickstat.cf:3306/quickstat";
	private static String driver = "com.mysql.jdbc.Driver";

	
	private DbHelper() throws SQLException {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		conn = DriverManager.getConnection(url, user, password);	
	}
	
	public static Connection getConnection() throws SQLException {
		if (conn == null) {
			new DbHelper();
		}
		return conn;
		
	}


	public static void closeConnection() {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
