package ru.braincoding.quickstat.crawler.db;

import java.util.Collection;

import ru.braincoding.quickstat.crawler.models.Page;
import ru.braincoding.quickstat.crawler.models.Person;
import ru.braincoding.quickstat.crawler.models.PersonPageRank;
import ru.braincoding.quickstat.crawler.models.Site;

public interface IDbStorage {

	/**
	 * Выбирает сайты из базы данных для которых еще нет ни одной страницы
	 * @return список (коллекцию) сайтов для которых нет ни одной страниы 
	 * или пустой список если таких сайтов нет
	 */
	Collection<Site> getNewSites();

	/**
	 * Выбирает из базы данных страницы у которых время последнего сканирования 
	 * равно 0000-00-00 00:00:00
	 * @param limit максимальное количество страниц
	 * @return список страниц или пустой список если таких страниц нет
	 */
	Collection<Page> getNewPages(int limit);

	/* Заглушка */
	Collection<Page> getPages(int limit);

	/**
	 * Метод для получения страниц которые обходились за некоторое количество дней 
	 * до настоящего момента
	 * @param days количество дней, такое что выбираются страницы у которых дата
	 * последнего сканирования раньше чем текущая дата минус число дней равное days
	 * @param limit максимальное число страниц
	 * @return возвращает список страниц у которых дата последнего сканирования 
	 * раньше чем за days дней до текущей
	 */
	Collection<Page> getPagesBefore(int days, int limit);

	/**
	 * Обновляет дату сканирования 
	 * @param pages список страниц
	 * @return количество страниц у которых была обновлена дата
	 */
	int updateLastScanDate(Collection<Page> pages);

	/**
	 * Выбирает личности из базы
	 * @return список личностей
	 */
	Collection<Person> getPersons();

	/**
	 * Выбирает из базы ключевые слова соответствующие личности person
	 * @param person личность для которой нужно выбрать ключевые слова
	 * @return список ключевых слов
	 */
	Collection<String> getKeyWords(Person person);

	/**
	 * Кандидат в deprecated
	 * Добавляет запись в таблицу test_person_page_rank
	 * @param person персона
	 * @param page страница
	 * @param rank рейтинг
	 * @return true если добавление прошло успешно, иначе false
	 */
	boolean updateRank(Person person, Page page, int rank);

	/**
	 * Добавляет записи в таблицу person_page_rank
	 * @param ranks список "рангов" {personId, pageID, rank}
	 * @return количество добавленных записей
	 */
	int updateRank(Collection<PersonPageRank> ranks);

	/**
	 * Добавляет страницы в базу данных
	 * @param pages список страниц для добавления
	 * @return количество добавленных страниц
	 */
	int addPages(Collection<Page> pages);

}