package ru.braincoding.quickstat.crawler;

public interface IDownloader {

	/**
	 * Returns a String containing downloaded page
	 * @param url the String contain page's URL
	 * @return the String contains downloaded page or empty String if any error
	 */
	String download(String url);

}