package ru.braincoding.quickstat.crawler;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import ru.braincoding.quickstat.crawler.Parser;

public class ValeryParser implements Parser {
	
/* Считаю что этот метод не совсем подходит для нашей задачи, потому что может 
 * посчитать одно и то же слово несколько раз, например при содержащемся в тексте
 * одном слове "Путина", он посчитает одно вхождение ключевого слова "Путин" 
 * и одно вхождение ключевого слова "Путина" т.е. в сумме 2 вхождения, а должно 
 * быть всего-лишь одно. Данный метод возможно был бы корректен, если бы команда 
 * пересмотрела подход к формированию ключевых слов, что как минимум требует 
 * обсуждения и некоторой переработки архитектуры софта (как краулера, так и, возможно,
 * других частей)
 * ! Сделано без преобразования страницы к нижнему регистру
 * иначе, например, при содержащемся в тексте слове "путинский" и ключевом слове "Путин", 
 * приведенном к нижнему регистру, данный метод посчитает это совпадением, что на 
 * мой взгляд, точно не является корректным поведением */
	@Override
	public int parse(String page, Collection<String> keyWords) {
        int keywordsCount = 0;
        for (String word : keyWords) {
			keywordsCount += StringUtils.countMatches(page, word);
        	
		}
		return keywordsCount;
	}

}
