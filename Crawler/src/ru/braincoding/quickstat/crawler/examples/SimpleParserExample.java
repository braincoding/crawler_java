package ru.braincoding.quickstat.crawler.examples;

import java.util.Arrays;
import java.util.Collection;

import ru.braincoding.quickstat.crawler.Downloader;
import ru.braincoding.quickstat.crawler.IDownloader;
import ru.braincoding.quickstat.crawler.SimpleParser;

public class SimpleParserExample {

	public static void main(String[] args) {
		Collection<String> kWords = Arrays.asList(args);
		IDownloader dwl = new Downloader();
		String result = dwl.download("http://regnum.ru/news/254767.html");
		int count = new SimpleParser().parse(result, kWords);
		System.out.println(count);

	}

}
