package ru.braincoding.quickstat.crawler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import ru.braincoding.quickstat.crawler.db.DbHelper;
import ru.braincoding.quickstat.crawler.db.DbStorage;
import ru.braincoding.quickstat.crawler.db.IDbStorage;
import ru.braincoding.quickstat.crawler.models.Page;
import ru.braincoding.quickstat.crawler.models.Person;
import ru.braincoding.quickstat.crawler.models.Site;
import ru.braincoding.quickstat.crawler.robots.RobotsParser;

public class Crawler {
	public static enum PAGE_TYPES {HTML, ROBOTS, SITEMAP, COMPRESSED_SM};
	private static IDbStorage storage = null;
	private static DocumentBuilderFactory docBuilderFactory = null;
	
	private static final int LIMIT = 1000;

	public static void main(String...args) {
		try {
			/* Получаем коннект к базе */
			storage = new DbStorage(DbHelper.getConnection());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* Получаем экземпляр фабрики парсеров для разбора ХМЛ
		 * и отменяем валидацию ХМЛ */
		docBuilderFactory = DocumentBuilderFactory.newInstance(); 
		docBuilderFactory.setValidating(false);
		
		/* Инициализируем парсер сайтмэп */
		SitemapParser.init();
		
		
		/* Получаем из базы сайты, которым не соответсвует ни одной страницы */
		Collection<Site> newSites = storage.getNewSites();
		
		for (Site site : newSites) {
			System.out.println(site.name);
		}
		
		
		if (!newSites.isEmpty()) {
			/* Если список не пуст, то каждому такому сайту генерируем по странице
			 * вида "http://" + site.name + "/robots.txt" 
			 * "https://" + site.name + "/robots.txt"  и доббавляем эти страницы в базу */
			Collection<Page> robotsPages = new ArrayList<>();
			for (Site site : newSites) {
				Page newPage = new Page();
				newPage.siteId = site.id;
				newPage.url = createRobotsHttp(site.name);  //я бы вот в этом методе сразу проверил доступность страницы, если нет такой - не загружать в базу нерабочую строку
				robotsPages.add(newPage);
				newPage = new Page();
				newPage.siteId = site.id;
				newPage.url = createRobotsHttps(site.name); //и тут тоже
				robotsPages.add(newPage);
			}
			storage.addPages(robotsPages);
		}
		/* Инстанцируем загрузчик страниц */
		IDownloader dwl = new Downloader();
		
		/* Получаем из базы страницы, у котрых поле last_scan_date равно 0000-00-00 00:00:00
		 * LIMIT - количество страниц, которые мы берем из базы*/
		Collection<Page> pages = storage.getNewPages(LIMIT);
		
		for (Page page : pages) {
			System.out.println(page.id + " : " + page.url);
		}
		
		while (!pages.isEmpty()) {
			/* Пока список не пуст выполняем следующее:
			 * 1. апдейтим last_scan_date у всех страниц */
			storage.updateLastScanDate(pages);
			
			/* 2. Для каждой страницы выполняем следующее: */
			for (Page page : pages) {
				/* 2.1 загружаем страницу */
				String file = dwl.download(page.url);
				
				/* 2.1 Определяем тип страниц */
				switch (selectType(page)) {
				case HTML:
					/* 2.1.1 Обычная страница - "Пишем в журнал"
					 * парсим все ключевые слова на этой странице */
					System.out.println("HTML: " + page.url);
					parseForAllPersons(file, page);
					break;
				case ROBOTS:
					/* 2.1.2 файл роботс.тхт - создаем список страниц
					 * в который запишем один пункт сайтмап, 
					 * если он будет найден на роботс.тхт
					 * ищем сайтмап.хмл с помощью RobotsParser.parse(file)
					 * добавляем этот сайтмап в базу */
					Collection<Page> robotsPages = new ArrayList<>();
					String sitemapUrl = RobotsParser.parse(file);
					if (!sitemapUrl.isEmpty()) {
						Page sitemapPage = new Page();
						sitemapPage.siteId = page.siteId;
						sitemapPage.url = sitemapUrl;
						robotsPages.add(sitemapPage);
						storage.addPages(robotsPages);
						
					}
					break;
				case SITEMAP:
					//TODO Create sitemap.xml parser class 
					
					/* 2.1.3 файл сайтмап.хмл - "Пишем в журнал" и создаем список страниц
					 * в который будем сохранять найденные на сайтмап.хмл страницы, 
					 * ищем страницы с помощью метода parseSiteMap(is, page) */
					System.out.println("SITEMAP: " + page.url);
					InputStream isRaw = new ByteArrayInputStream(file.getBytes());
					Collection<Page> sitemapPages = SitemapParser.parseSiteMap(isRaw, page);
					storage.addPages(sitemapPages);
					break;
					
				case COMPRESSED_SM:
					
					/* 2.1.3 сжатый файл сайтмап.хмл.гз - 
					 * получаем поток, парсим  
					 *  */
					System.out.println("SITEMAP.GZ: " + page.url);
					InputStream isCompessed = getIS(page.url);
					Collection<Page> sitemapGzPages = SitemapParser.parseSiteMap(isCompessed, page);
					storage.addPages(sitemapGzPages);
					
					break;
				}
			}
			
			/* 3. Получаем новый список страниц из базы */
			pages = storage.getNewPages(LIMIT);
			for (Page page2 : pages) {
				System.out.println("new : " + page2.url);
			}
		}
		
		/* Получаем из базы список "старых" страниц */
		pages = storage.getPagesBefore(1, LIMIT);
		
		for (Page page2 : pages) {
			System.out.println(page2.url);
		}
		
		while (!pages.isEmpty()) {
			/* Пока список не пуст выполняем следующее:
			 * 1. апдейтим last_scan_date у всех страниц */
			storage.updateLastScanDate(pages);
			
			/* 2. для всех страниц выполняем следующее: */
			for (Page page : pages) {
				System.out.println(page.id + " : " + page.url);
				/* 2. если страница - сайтмап, то скачиваем ее и парсим
				 * собственно логика работы краулера описаннная в вики проекта
				 * не предполагает повторного обхода самих новостей, а предлагает только
				 * повторный парсинг сайтмап */
				
				// TODO Переписать через if
				switch (selectType(page)) {
				case SITEMAP:
					String file = dwl.download(page.url);
					Collection<Page> sitemapPages = parseSiteMap(file, page);
					storage.addPages(sitemapPages);
					break;
					
				case COMPRESSED_SM:
					
					/* 2.1.3 сжатый файл сайтмап.хмл.гз - 
					 * получаем инпут стрим, и парсим из потока,  
					 *  */
					InputStream is = getIS(page.url);

					Collection<Page> sitemapGzPages = parseSiteMap(is, page);
					storage.addPages(sitemapGzPages);
					
					break;

				default:
					break;
				}
			}
			pages = storage.getPagesBefore(1, LIMIT);
		}
		
		
	}
	
	
	
	/* createRobotsHttps(String name) и createRobotsHttp(String name)
	 * это нечто вроде заглушек возможно в будущем нужно будет переписать
	 * и сделать один метод, так чтобы он проверял какой именно роботс.тхт
	 * доступен и выдавал только одну страницу */
	private static String createRobotsHttps(String name) {
		return "https://" + name + "/robots.txt";
	}
	
	
	private static String createRobotsHttp(String name) {
		
		return "http://" + name + "/robots.txt";
	}

	
	private static Collection<Page> parseSiteMap(String download, Page srcPage) {
		
		
	    DocumentBuilder db;
	    ByteArrayInputStream src = new ByteArrayInputStream(download.getBytes());
	    Collection<Page> result = new ArrayList<>();
		try {
			
			db = docBuilderFactory.newDocumentBuilder();
			/* создаем ХМЛ документ из скачанной страницы */
			Document doc = db.parse(src); 
			
			/* берем из документа все ноды с тегом "loc" */
			NodeList nodes = doc.getElementsByTagName("loc");
			
			/* снабжаем адреса страниц соответствующим siteId и
			 * закидываем адреса страниц в результирующий список  */
			for (int i = 0; i < nodes.getLength(); i++) {
				Page page = new Page();
				page.siteId = srcPage.siteId;
				page.url = nodes.item(i).getTextContent().trim();
				result.add(page);
				System.out.println(page.url);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		/* возвращаем результат */
		return result;

		// TODO LOG
		
	}
	
	private static Collection<Page> parseSiteMap(InputStream src, Page srcPage) {
		
		
	    DocumentBuilder db;
	    Collection<Page> result = new ArrayList<>();
		try {
			
			db = docBuilderFactory.newDocumentBuilder();
			/* создаем ХМЛ документ из скачанной страницы */
			Document doc = db.parse(src); 
			
			/* берем из документа все ноды с тегом "loc" */
			NodeList nodes = doc.getElementsByTagName("loc");
			
			/* снабжаем адреса страниц соответствующим siteId и
			 * закидываем адреса страниц в результирующий список  */
			for (int i = 0; i < nodes.getLength(); i++) {
				Page page = new Page();
				page.siteId = srcPage.siteId;
				page.url = nodes.item(i).getTextContent().trim();
				result.add(page);
				System.out.println(page.url);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		/* возвращаем результат */
		return result;

		// TODO LOG
		
	}

	/* Метод парсит все ключевые слова по тексту страницы и обновляет БД
	 * тут точно нужно переписать.
	 * Во-первых лучше сделать отельным классом
	 * во-вторых сделать так чтобы метод возвращал список person_page_rank
	 * и не обращался к БД.
	 * возвращенный списол лучще записывать в базу потом */
	private static void parseForAllPersons(String file, Page page) {
		/* создаем самописный парсер ключевых слов */
		Parser parser = new SimpleParser();
		/* выбираем личности из базы */
		Collection<Person> persons = storage.getPersons();
	//TODO Log
		/* для каждой личности */
		for (Person person : persons) {
			/* для каждой личности 
			 * "пишем в журнал" и выбираем ключевые слова из базы*/
			System.out.println(person.name + " : " + person.id);
			Collection<String> keyWords = storage.getKeyWords(person);
			
			/* считаем упоминания */
			int rank = parser.parse(file, keyWords);
			if (rank > 0) {
				/* если есть упоминание записываем результат в базу */
				storage.updateRank(person, page, rank);
			}
			/* "пишем в журнал" */
			System.out.println(Arrays.toString(keyWords.toArray()) + " : " + rank);
		}
		
	}
	
	
	/* определитель типа страницы, код тривиальный
	 * определяе страницу по расширению:
	 * по умолчанию HTML
	 * если xml то SITEMAP (sitemap.xml)
	 * если txt то ROBOTS (robots.txt)
	 * если gz то COMPRESSED_SM (сжатый сайтмап sitemap.xml.gz)*/
	public static PAGE_TYPES selectType(Page page) {
		PAGE_TYPES result = PAGE_TYPES.HTML;
		if (page.url.endsWith("xml")){
			result = PAGE_TYPES.SITEMAP;
			
		} else if (page.url.endsWith("txt")) {
			result = PAGE_TYPES.ROBOTS;
		} else if (page.url.endsWith("gz")) {
			result = PAGE_TYPES.COMPRESSED_SM;
		}
			
		return result;
	}
	
	private static InputStream getIS(String url) {
		GZIPInputStream gzStream = null;
		try {
			URL locator = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) locator.openConnection();
			gzStream = new GZIPInputStream(conn.getInputStream());

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return gzStream;
		
	}

}
