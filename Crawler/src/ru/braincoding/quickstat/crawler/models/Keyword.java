package ru.braincoding.quickstat.crawler.models;

public class Keyword {
	public static final String TABLE = "keywords";
	public static class Columns {
		public static final String ID = "id";
		public static final String NAME = "name";
		public static final String PERSON_ID = "person_id";
	}
	
	public long id;
	public String name;
	public long personId;

}
