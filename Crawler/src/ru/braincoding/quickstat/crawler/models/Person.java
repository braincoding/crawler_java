package ru.braincoding.quickstat.crawler.models;

public class Person {
	public static final String TABLE = "persons";
	public static class Columns {
		public static final String ID = "id";
		public static final String NAME = "name";
	}
	
	public long id;
	public String name;

}
