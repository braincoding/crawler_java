package ru.braincoding.quickstat.crawler.models;

import java.sql.Timestamp;

public class Page {
	
	public static final String DEFAULT_CONTENT = "";
	public static final String TABLE = "pages";
	public static class Columns {
		public static final String ID = "id";
		public static final String URL = "url";
		public static final String SITE_ID = "site_id";
		public static final String FOUND = "found_date_time";
		public static final String SCAN = "last_scan_date";
		public static final String HASH = "hash";
	}

	public long id;
	public String url;
	public long siteId;
	public Timestamp foundDateTime;
	public Timestamp lastScanDate;
	public String content = DEFAULT_CONTENT;

}
