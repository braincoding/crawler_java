package ru.braincoding.quickstat.crawler.models;

public class Site {
	public static final String TABLE = "sites";
	public static class Columns {
		public static final String ID = "id";
		public static final String NAME = "name";
	}
	
	public long id;
	public String name;

}
