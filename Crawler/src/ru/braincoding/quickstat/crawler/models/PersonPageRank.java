package ru.braincoding.quickstat.crawler.models;

public class PersonPageRank {
	public static final String TABLE = "person_page_rank";
	public static class Columns {
		public static final String ID = "id";
		public static final String PERSON_ID = "person_id";
		public static final String PAGE_ID = "page_id";
		public static final String RANK = "rank";
	}
	
	public long id;
	public long personId;
	public long pageId;
	public int rank;

}
