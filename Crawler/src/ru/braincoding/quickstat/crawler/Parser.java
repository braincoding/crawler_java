package ru.braincoding.quickstat.crawler;

import java.util.Collection;

public interface Parser {
	public int parse(String page, Collection<String> keyWords);

}
